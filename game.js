/**
 * Baccarat Game V1.0.0
 * Credit by Ake 
 * 
 **/


var row = 0;
var col = 0;
var rowMax = 6;
var colMax = 12;
var end_game = true;
var userClick = 0;
var botSelect = '';
var botAdvice = '';
var score = [];
var handIndex = 0;
var clickLose = 0;
//var items = ['btn']


$(function () {

    beginGame();
    $(".new_game").bind('click', function () {
        if (userClick == 0 || userClick && confirm('ต้องการเริ่มเกมส์ใหม่')) {
            startPlayer();
        }
    });

});

function beginGame() {
    $(".play").unbind('click');
    $(".play").addClass('disabled');
    $('div#board-col .selected');
}


function reset() {
    row = 0;
    col = 0;
    end_game = false;
    userClick = 0;
    botSelect = '';
    botAdvice = '';
    score = [];
    handIndex = 0;
    bindScore();
    $('.res-alert').hide();

    $(".play").removeClass('disabled');
    $('div.board-col > .selected').hide();
    $(".play").unbind('click');
}

function bindPrev() {
    if (row == 0 && col == 0) {
        $(".prev_game").attr('disabled', true);
        console.log('row:' + row + 'col:' + col);
    } else {
        $(".prev_game").attr('disabled', false);
        $(".prev_game").unbind('click');
        $(".prev_game").bind('click', function () {
            prevKey();
        });
    }
}


function startPlayer() {
    reset();
    bindPrev();
    $(".play").bind('click', function () {
        if (!end_game) {
            var valSelect = $(this).val();
            $('div#cell-' + row + '-' + col + ' .selected').addClass(items[valSelect]['value']).show();
            getKey();
        } else {
            alert('End Game');
            beginGame();
        }

        ++userClick;
        $('.userClick').text(userClick);

        //if (userClick % 3 == 0) {
        if (userClick == 3) {

            //console.log(item);
            //  $('div#cell-' + row + '-' + col + ' .selected').addClass(item['value']).show();
            //  getKey();
            //$('.bot').append(item['color'] + ",");

            //botAdvice = item;
            randomItems();
            // console.log('Bot Advice:');
            // console.log(botAdvice);
            ++handIndex;
            $('.res-alert').removeClass('alert-danger alert-success').addClass('alert-info');
            $('.res-alert').html('ไม้ที่ ' +
                    handIndex + " <button class='btn " + botAdvice['value'] + "'>" + botAdvice['title'] + "</button>"
                    ).show();
        } else if (valSelect == botAdvice['id']) {

            score.push({'handIndex': handIndex, 'res': true});
            handIndex = 1;
            //botAdvice = items[Math.floor(Math.random() * ranItems.length)];
            randomItems();
            $('.res-alert').removeClass('alert-danger').addClass('alert-success');
            $('.res-alert').html('สูตรสำเร็จ<br/><h2>ยินดีด้วยครับ</h2> ' +
                    'ไม้ที่ ' +
                    handIndex + " <button class='btn " + botAdvice['value'] + "'>" + botAdvice['title'] + "</button>"
                    ).show();
            bindScore();

        } else if (botAdvice != '' && valSelect != botAdvice['id']) {
            
            if (handIndex == 3) {
                score.push({'handIndex': handIndex, 'res': false});
                handIndex = 1;
                $('.res-alert').removeClass('alert-danger alert-success').addClass('alert-info');
                $('.res-alert').text("แนะนำให้เลือกห้องใหม่").show();
                
            } else {
                ++handIndex;
                $('.res-alert').removeClass('alert-success').addClass('alert-danger');
                $('.res-alert').html('ลองอีกครั้ง<br/><h2>ไม้ต่อไป</h2> ' +
                        'ไม้ที่ ' +
                        handIndex + " <button class='btn " + botAdvice['value'] + "'>" + botAdvice['title'] + "</button>"
                        ).show();
                //bindScore();
            }
            bindScore();


        } else {
            $('.res-alert').removeClass('alert-danger alert-success').addClass('alert-info');
            $('.res-alert').text("รอสูตร").show();
            bindScore();
        }

        //  score.push({ 'click': userClick });
        //  console.log(score);
        bindPrev();
    });
}

function randomItems() {
    var id = Math.floor(Math.random() * items.length);
    if (id != 1) {
        botAdvice = items[id];
    } else {
        randomItems();
    }
    console.log('botAdvice:');
    console.log(botAdvice);
}

function bindScore() {
    var win = 0;
    var lose = 0;
    console.log(score);
    $('table.score tbody').empty();
    $(score).each(function (key, value) {
        if (value['res']) {
            res = '<span class="label label-success">ชนะ</span>'
        } else {
            res = '<span class="label label-danger">แพ้</span>'
        }
        $('table.score tbody').append("<tr><td>" + (key + 1) + "</td><td>" + (value['handIndex']==3?'-':value['handIndex']) + "</td><td>" + res + "</td></tr>");
        if (value['res']) {
            ++win;
        } else {
            ++lose;
        }
    });

    $('table.sammary tbody').empty();
    $('table.sammary tbody').append("<tr><td>" + (win + lose) + "</td><td>" + win + "</td><td>" + lose + "</td></tr>");


}


function endGame() {
    $(".play").unbind('click');
    $(".play").addClass('disabled');
    //$('div#cell-' + row + '-' + col + ' .selected');
}

function getKey() {
    //console.log('Before row:' + row + "col:" + col);
    if (row < rowMax) {
        ++row;
    } else {
        if (col < colMax) {
            ++col;
        } else {
            end_game = true;
            beginGame();
        }
        row = 0;
    }
    //console.log('After row:' + row + "col:" + col);
}

function prevKey() {
    console.log('Before row:' + row + "col:" + col);
    console.log('div#cell-' + row + '-' + col + ' .selected');
    
    if (row > 0) {
        --row;
    } else {
        if (col > 0) {
            --col;
        } else {
            end_game = false;
            beginGame();            
        }
        row = rowMax;
    }
    
    $('div.board-col#cell-' + row + '-' + col + ' .selected').removeClass('btn-primary btn-success btn-danger').hide();

    score.splice(-1, 1);
    bindScore();
    console.log('After row:' + row + "col:" + col);
}
 